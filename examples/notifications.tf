module "sns_topic_devops-email" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-sns.git?ref=0.12"
  name   = "DevOps-Email"
  subscribers = {
    alertas-cloudwatch-projuris = {
      protocol               = "email"
      endpoint               = "xxxxxxx"
      endpoint_auto_confirms = false
    }
  }
  policy = <<EOF
{
  "Version": "2008-10-17",
  "Id": "__default_policy_ID",
  "Statement": [
    {
      "Sid": "__default_statement_ID",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish"
      ],
      "Resource": "arn:aws:sns:us-east-1:111111111111:DevOps-Email",
      "Condition": {
        "StringEquals": {
          "AWS:SourceOwner": "111111111111"
        }
      }
    },
    {
      "Sid": "BackupPublishEvents",
      "Effect": "Allow",
      "Principal": {
        "Service": "backup.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:111111111111:DevOps-Email"
    }
  ]
}
EOF
}

module "s3-example-test" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-s3-bucket.git?ref=0.12"
  name   = "example-test"
  versioning = {
    enabled = true
  }
  tags = {
    backup_plan = "test-01"
  }
}

module "backup_plan-test-01" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-backup.git?ref=0.12"
  name   = "test-01"

  rules = [
    {
      rule_name = "Daily"
      schedule  = "cron(0 14 ? * * *)"
      lifecycle = {
        delete_after = "90"
      }
    },
    {
      rule_name                = "Daily-PITR"
      schedule                 = "cron(0 14 ? * * *)"
      enable_continuous_backup = true
      lifecycle = {
        delete_after = "7"
      }
    },
  ]

  selections = [
    {
      name = "default"
      selection_tags = [
        {
          type  = "STRINGEQUALS"
          key   = "backup_plan"
          value = "test-01"
        },
      ]
    },
  ]

  notifications = {
    sns_topic_arn       = module.sns_topic_devops-email.this_sns_topic_arn
    backup_vault_events = ["BACKUP_JOB_EXPIRED", "BACKUP_JOB_FAILED", "COPY_JOB_FAILED", "RESTORE_JOB_FAILED"]
  }
}
