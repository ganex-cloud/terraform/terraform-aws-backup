locals {
  selection = var.selection_name == null ? [] : [
    {
      name           = var.selection_name
      resources      = var.selection_resources
      not_resources  = var.selection_not_resources
      conditions     = var.selection_conditions
      selection_tags = var.selection_tags
    }
  ]

  selections = concat(local.selection, var.selections)

  recovery_point_tags = {
    backup_plan_name = var.name
    backup_vault     = var.name
  }

  iam_policies = [
    "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup",
    "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores",
    "arn:aws:iam::aws:policy/AWSBackupServiceRolePolicyForS3Backup"
  ]
}
