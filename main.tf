data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }

    dynamic "condition" {
      for_each = var.create_backup_vault ? list(1) : []
      content {
        test     = "ArnLike"
        variable = "aws:SourceArn"
        values   = [aws_backup_vault.default[0].arn]
      }
    }

    condition {
      test     = "StringEquals"
      variable = "aws:SourceAccount"
      values   = [data.aws_caller_identity.current.account_id]
    }
  }
}

resource "aws_iam_role" "default" {
  count              = var.create_iam_role ? 1 : 0
  name               = "Backup-${var.name}"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
  path               = "/service-role/"
}

resource "aws_iam_role_policy_attachment" "default" {
  count      = var.create_iam_role && length(local.iam_policies) > 0 ? length(local.iam_policies) : 0
  role       = aws_iam_role.default[0].name
  policy_arn = local.iam_policies[count.index]
}

resource "aws_iam_role_policy_attachment" "s3" {
  count      = var.create_iam_role ? 1 : 0
  role       = aws_iam_role.default[0].name
  policy_arn = aws_iam_policy.s3[0].arn
}

resource "aws_iam_policy" "s3" {
  count  = var.create_iam_role ? 1 : 0
  name   = "AWSBackupServiceRolePolicyForS3BackupAdditional-${var.name}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "BackupPolicy",
      "Effect": "Allow",
      "Action": [
        "s3:GetBucketNotification"
      ],
      "Resource": "arn:aws:s3:::*"
    }
  ]
}
EOF
}

resource "aws_backup_vault" "default" {
  count       = var.create_backup_vault ? 1 : 0
  name        = var.name
  kms_key_arn = var.vault_kms_key_arn
  tags        = var.tags
}

resource "aws_backup_plan" "default" {
  count = var.create_backup_plan ? 1 : 0
  name  = var.name
  tags  = var.tags

  # Rules
  dynamic "rule" {
    for_each = var.rules
    content {
      rule_name         = lookup(rule.value, "rule_name", null)
      target_vault_name = lookup(rule.value, "target_vault_name", null) != null ? rule.value.target_vault_name : var.name
      # target_vault_name        = lookup(rule.value, "target_vault_name", null)
      schedule                 = lookup(rule.value, "schedule", null)
      start_window             = lookup(rule.value, "start_window", null)
      completion_window        = lookup(rule.value, "completion_window ", null)
      enable_continuous_backup = lookup(rule.value, "enable_continuous_backup", null)
      recovery_point_tags      = length(lookup(rule.value, "recovery_point_tags", {})) == 0 ? local.recovery_point_tags : lookup(rule.value, "recovery_point_tags")

      # Lifecycle  
      dynamic "lifecycle" {
        for_each = length(lookup(rule.value, "lifecycle")) == 0 ? [] : [lookup(rule.value, "lifecycle", {})]
        content {
          cold_storage_after = lookup(lifecycle.value, "cold_storage_after", 0)
          delete_after       = lookup(lifecycle.value, "delete_after", 90)
        }
      }

      # Copy action
      dynamic "copy_action" {
        for_each = length(lookup(rule.value, "copy_action", {})) == 0 ? [] : [lookup(rule.value, "copy_action", {})]
        content {
          destination_vault_arn = lookup(copy_action.value, "destination_vault_arn", null)

          # Copy Action Lifecycle
          dynamic "lifecycle" {
            for_each = length(lookup(copy_action.value, "lifecycle", {})) == 0 ? [] : [lookup(copy_action.value, "lifecycle", {})]
            content {
              cold_storage_after = lookup(lifecycle.value, "cold_storage_after", 0)
              delete_after       = lookup(lifecycle.value, "delete_after", 90)
            }
          }
        }
      }
    }
  }
  depends_on = [aws_backup_vault.default]
}
