resource "aws_backup_vault_notifications" "this" {
  count               = length(var.notifications) > 0 ? 1 : 0
  backup_vault_name   = var.name
  sns_topic_arn       = lookup(var.notifications, "sns_topic_arn", null)
  backup_vault_events = lookup(var.notifications, "backup_vault_events", [])
}
