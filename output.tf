output "plan_name" {
  value       = var.create_backup_plan ? join("", aws_backup_vault.default.*.name) : null
  description = "The name of backup plan."
}

output "plan_id" {
  value       = var.create_backup_plan ? join("", aws_backup_plan.default.*.id) : null
  description = "The ID of backup plan."
}

output "vault_name" {
  value       = var.create_backup_vault ? join("", aws_backup_vault.default.*.name) : null
  description = "The name of vault."
}

output "vault_id" {
  value       = var.create_backup_vault ? join("", aws_backup_vault.default.*.id) : null
  description = "The id of vault."
}

output "role_arn" {
  value       = var.create_iam_role ? element(concat(aws_iam_role.default.*.arn, [""]), 0) : null
  description = "ARN of IAM role."
}

