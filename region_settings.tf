resource "aws_backup_region_settings" "this" {
  count                               = length(var.region_settings) > 0 ? 1 : 0
  resource_type_opt_in_preference     = lookup(var.region_settings, "resource_type_opt_in_preference", null)
  resource_type_management_preference = lookup(var.region_settings, "resource_type_management_preference", null)
}
