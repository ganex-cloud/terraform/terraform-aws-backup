variable "create_backup_vault" {
  description = "(Required) Whether to create backup vault."
  type        = bool
  default     = true
}

variable "create_backup_plan" {
  description = "(Required) Whether to create backup plan."
  type        = bool
  default     = true
}

variable "create_iam_role" {
  description = "(Required) Whether to create iam role."
  type        = bool
  default     = true
}

variable "name" {
  description = "(Required) The display name of a backup plan."
  type        = string
  default     = null
}

variable "rules" {
  description = "(Required) A rule object that specifies a scheduled task that is used to back up a selection of resources."
  type        = any
  default     = []
}

variable "vault_kms_key_arn" {
  description = "(Optional) The server-side encryption key that is used to protect your backups."
  type        = string
  default     = null
}

variable "tags" {
  description = "(Optional) Metadata that you can assign to help organize the resources that you create."
  type        = map(string)
  default     = {}
}

variable "selections" {
  description = "(Required) A list of selections maps."
  type        = any
  default     = []
}

variable "selection_name" {
  description = "The display name of a resource selection document"
  type        = string
  default     = null
}

variable "selection_resources" {
  description = "An array of strings that either contain Amazon Resource Names (ARNs) or match patterns of resources to assign to a backup plan"
  type        = list(any)
  default     = []
}

variable "selection_not_resources" {
  description = "An array of strings that either contain Amazon Resource Names (ARNs) or match patterns of resources to exclude from a backup plan."
  type        = list(any)
  default     = []
}

variable "selection_conditions" {
  description = "A map of conditions that you define to assign resources to your backup plans using tags."
  type        = map(any)
  default     = {}
}

variable "selection_tags" {
  description = "List of tags for `selection_name` var, when using variable definition."
  type        = list(any)
  default     = []
}

variable "region_settings" {
  description = "(Optional) Provides an AWS Backup Region Settings resource.."
  type        = any
  default     = {}
}

variable "notifications" {
  description = "(Optional) An array of events that indicate the status of jobs to back up resources to the backup vault."
  type        = any
  default     = {}
}
